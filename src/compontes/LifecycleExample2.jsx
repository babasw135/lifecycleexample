import React, { useState } from 'react';

function LifecycleExample() {
    const [count, setCount] = useState(0);
        //count는 값이고 setCount는 변수 초기값은 0.

    React.useEffect(() => {
        console.log('Component mounted');
        return () => {
            console.log('Component will unmount');
        };
    }, []);

    React.useEffect(() => {
        console.log('Count updated:', count);
    }, [count]);

    const handleIncrement = () => { //상태 업데이트 함수
        setCount((prevCount) => prevCount + 1);
    };

    return (
        <div>
            <p>Count: {count}</p>
            <button onClick={handleIncrement}>Increment</button>
        </div>
    );
}

export default LifecycleExample;