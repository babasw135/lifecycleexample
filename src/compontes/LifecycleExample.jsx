import React, { Component } from 'react';

class LifecycleExample extends Component {
    constructor(props) { //생성자 , 첫번째 실행
        super(props); // super 부모가 Component 부모님은 우리의 수퍼맨. 항상 부모에게 먼저 준다.
        this.state = { // constructor입장에서 this의 위치는 Component 전체를 말한다. 말하고 있는 주체를 알아야 여기가 어디인지 알 수 있다. state 는 어디에도 없다. 맴버변수가 없으면 동적언어는 만들어 버린다.
            count: 0, // state의 초기상태는 0 이다. js에서는 객체라고 부른다. json,mae과 같다.
        };
    }
    componentDidMount() {
        console.log('Component mounted');//컴포넌트가 마운트 된 직후 메세지 호출
    }

    componentDidUpdate(prevProps, prevState) {//Props는 count가 되고 State 0이 된다.
        //이 메서드는 컴포넌트가 업데이트된 직후(즉, 상태나 속성이 변경된 직후)에 호출됩니다.
        // 여기서는 this.state.count가 이전 props.count와 다를 때마다 'Count updated' 메시지를 로그로 출력합니다.
        if (this.state.count !== prevState.count){
            console.log('Count updated', this.state.count);
        }
    }

    componentWillUnmount = () => { //이 메서드는 컴포넌트가 언마운트되기 직전에 호출됩니다.
        console.log('Component will unmount');
    }

    handleIncrement = () => { //이 메서드는 버튼 클릭 시 호출되어 count 상태를 업데이트합니다.
        this.setState((prevState) => ({count: prevState.count + 1}));
        // 1웨이 바인딩이라 setState 해줘야 저장이 됨
    };

    render() { // 두번째 실행 순서
        return(
            <div>
                <p>Count: {this.state.count}</p>
                <button onClick={this.handleIncrement}>Increment</button>
            </div>
            //값을 초기화 후
            //이 메서드는 컴포넌트의 UI를 렌더링합니다.
            // 현재 상태의 count 값을 표시하고, 버튼을 클릭하면 handleIncrement 메서드를 호출합니다.
        );
    }
}

export default LifecycleExample;