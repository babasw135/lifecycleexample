
import './App.css';
import LifecycleExample from "./compontes/LifecycleExample";

function App() {
  return (
    <div className="App">
     <LifecycleExample/>
    </div>
  );
}

export default App;
